<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Client;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth', ['except' => ['login','index']]);
    }

    public function index(Request $request){
        $client = new Client;
        
        try{
            if($request->hasFile('image')){
                $nombreImagen = $request ->file('image')->getClientOriginalName();
                $carpetaDestino ='./upload/';
                $request->file('image')->move($carpetaDestino,$nombreImagen);
                
                $client->email = $request->email;
                $client->username = $request->username;
                $client->password = app('hash')->make($request->password);
                $client->image = $nombreImagen;
                
                $client->save();
            }
            return array(
                'success' => true,
                'message' => "$client->username has been created.",
                'response' => $client
            );
        }catch(\Exception $ex) {
            return array(
                'success' => false,
                'message' => 'Internal server error.',
                'response' => $ex
            );
        }
    }
    
    public function login(Request $request){
        try{
            if($request->password && $request->username) {                
                $this->validate($request, [
                    'username' => 'required',
                    'password' => 'required',
                ]);
                $credentials = $request->only('username', 'password');
                $token = JWTAuth::attempt($credentials);
                    if (!$token) {
                        return response()->json(['error' => 'Unauthorized','response'=>$token], 401);
                    }else{
                        return response()->json([
                            'success' => true,
                            'message' => 'Login in',
                            'response' => $token
                        ]);
                    }
            } else {    
                return response()->json([
                    'success' => false,
                    'message' => 'Login error',
                    'response' => null
                ]);
            }
            if($request->password && $request->email) {                
                $this->validate($request, [
                    'email' => 'required',
                    'password' => 'required',
                ]);
                $credentials = $request->only('email', 'password');
                $token = JWTAuth::attempt($credentials);
                    if (!$token) {
                        return response()->json(['error' => 'Unauthorized','response'=>$token], 401);
                    }else{
                        return response()->json([
                            'success' => true,
                            'message' => 'Login in',
                            'response' => $token
                        ]);
                    }
            } else {    
                return response()->json([
                    'success' => false,
                    'message' => 'Login error',
                    'response' => null
                ]);
            }
        }catch(\Exception $ex) {
            return array(
                'success' => false,
                'message' => 'Internal server error.',
                'response' => $ex
            );
        }
    }
}