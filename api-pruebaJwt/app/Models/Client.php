<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model{
    protected $table = "users";
    protected $primaryKey = 'idClient';

    protected $fillable = [
        'idClient',
        'email',
        'username',
        'password',
        'image'
    ];

    protected $hidden = [
        'active'
    ];
   // public $timestamps = true;
}